package com.pearson.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class BitBucketTest1Application extends SpringBootServletInitializer {
		
		@Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(BitBucketTest1Application.class);
	    }

	public static void main(String[] args) {
		SpringApplication.run(BitBucketTest1Application.class, args);
	}
}
