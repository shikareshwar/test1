package com.pearson.demo.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello World";

	@RequestMapping(value = "/", consumes = MediaType.ALL_VALUE)
	public String home(Model model) {

		
		model.addAttribute("message", message);
		return "lrSearch";
	}

	

}
